Lite Auto Login
================

Lite Auto Login is a simple GUI to enable or disable logging in.

## Screenshots:

![](https://imgur.com/VmKN7AE.png)

![](https://imgur.com/kDH5GMG.png)

## License ![License](https://img.shields.io/badge/license-GPLv2-green.svg)

This project is under the GPLv2 license. Unless otherwise stated in individual files.

## Authors
- [Jerry Bezencon](https://gitlab.com/linuxlite/)
- [Ralphy](https://github.com/ralphys/)
